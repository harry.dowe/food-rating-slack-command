<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Blocks\DividerBlock;
use Tests\TestCase;

class DividerBlockTest extends TestCase
{
    public function testItImplementsSlackBlock(): void
    {
        $this->assertInstanceOf(SlackBlock::class, new DividerBlock());
    }

    public function testItTransformsToArray(): void
    {
        $divider = new DividerBlock();

        $this->assertSame([
            'type' => 'divider',
        ], $divider->toArray());
    }
}
