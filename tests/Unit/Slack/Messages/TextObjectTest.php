<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Messages;

use App\Slack\Messages\TextObject;
use Tests\TestCase;

class TextObjectTest extends TestCase
{
    public function testItTransformsToArrayWithMarkdownAsDefault(): void
    {
        $text = new TextObject('hello');

        $this->assertSame([
            'type' => 'mrkdwn',
            'text' => 'hello',
        ], $text->toArray());
    }

    public function testItTransformsToArrayWithMarkdown(): void
    {
        $text = new TextObject('hello', 'mrkdwn');

        $this->assertSame([
            'type' => 'mrkdwn',
            'text' => 'hello',
        ], $text->toArray());
    }

    public function testItTransformsToArrayWithPlainText(): void
    {
        $text = new TextObject('hello', 'plain_text');

        $this->assertSame([
            'type' => 'plain_text',
            'text' => 'hello',
        ], $text->toArray());
    }

    public function testItThrowsInvalidArgumentExceptionWithInvalidType(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new TextObject('', 'invalid-type');
    }
}
