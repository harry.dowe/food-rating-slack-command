<?php declare(strict_types=1);

namespace App\FoodHygiene;

class RepositoryException extends \Exception
{
}
