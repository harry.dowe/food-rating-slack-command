<?php declare(strict_types=1);

namespace Tests\Gateway;

class Request
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $body;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var array
     */
    private $queryStringParameters;

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    private function hasPath(): bool
    {
        return isset($this->path);
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    private function hasMethod(): bool
    {
        return isset($this->method);
    }

    /**
     * @param mixed $json
     */
    public function setJson($json): self
    {
        $this->body = [
            'type' => 'JSON',
            'json' => json_encode($json),
            'matchType' => 'STRICT',
        ];

        return $this;
    }

    private function hasBody(): bool
    {
        return isset($this->body);
    }

    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    private function hasHeaders(): bool
    {
        return !empty($this->headers);
    }

    public function setQueryStringParameters(array $params): self
    {
        $this->queryStringParameters = $params;

        return $this;
    }

    private function hasQueryStringParameters(): bool
    {
        return isset($this->queryStringParameters);
    }

    public function build(): array
    {
        $request = [];

        if ($this->hasPath()) {
            $request['path'] = $this->path;
        }

        if ($this->hasMethod()) {
            $request['method'] = $this->method;
        }

        if ($this->hasBody()) {
            $request['body'] = $this->body;
        }

        if ($this->hasHeaders()) {
            $request['headers'] = MockserverHelper::mapKeyToMultiValue($this->headers);
        }

        if ($this->hasQueryStringParameters()) {
            $request['queryStringParameters'] = MockserverHelper::mapKeyToMultiValue($this->queryStringParameters);
        }

        return $request;
    }
}
