<?php declare(strict_types=1);

namespace App\Slack\Blocks;

use App\Slack\Block\SlackBlock;

class DividerBlock implements SlackBlock
{
    private const TYPE = 'divider';

    public function toArray(): array
    {
        return [
            'type' => self::TYPE,
        ];
    }
}
