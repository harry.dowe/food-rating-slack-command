<?php declare(strict_types=1);

namespace Tests\Unit\Slack;

use App\Slack\RequestSigningValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class RequestSigningValidatorTest extends TestCase
{
    public function testItValidatesRequest(): void
    {
        $validator = new RequestSigningValidator('some-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(5)->subSecond());

        $request = new Request(['query' => 'value', 'king' => 'queen'], [], [], [], [], [
            'HTTP_X-Slack-Signature' => 'v0=f928666dc3e47e1d919cf3639a171bc8d6e72028bddc393544ae80c81fcc4817',
            'HTTP_X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $this->assertTrue($validator->validate($request));
    }

    public function testItValidatesPostRequests(): void
    {
        $validator = new RequestSigningValidator('some-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(5)->subSecond());

        $request = new Request([], ['query' => 'value', 'king' => 'queen'], [], [], [], [
            'HTTP_X-Slack-Signature' => 'v0=f928666dc3e47e1d919cf3639a171bc8d6e72028bddc393544ae80c81fcc4817',
            'HTTP_X-Slack-Request-Timestamp' => 1553249818,
            'REQUEST_METHOD' => 'POST',
        ]);

        $this->assertTrue($validator->validate($request));
    }

    public function testItInvalidatesRequestWhenSuspectedReplayAttack(): void
    {
        $validator = new RequestSigningValidator('some-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(5));

        $request = new Request(['query' => 'value', 'king' => 'queen'], [], [], [], [], [
            'HTTP_X-Slack-Signature' => 'v0=f928666dc3e47e1d919cf3639a171bc8d6e72028bddc393544ae80c81fcc4817',
            'HTTP_X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $this->assertFalse($validator->validate($request));
    }

    public function testItInvalidatesRequestWhenMissingSignatureHeader(): void
    {
        $validator = new RequestSigningValidator('some-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(4));

        $request = new Request([], [], [], [], [], [
            'HTTP_X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $this->assertFalse($validator->validate($request));
    }

    public function testItInvalidatesRequestWhenMissingTimestampHeader(): void
    {
        $validator = new RequestSigningValidator('some-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(4));

        $request = new Request([], [], [], [], [], [
            'HTTP_X-Slack-Signature' => 'v0=f928666dc3e47e1d919cf3639a171bc8d6e72028bddc393544ae80c81fcc4817',
        ]);

        $this->assertFalse($validator->validate($request));
    }

    public function testItInvalidatesRequestWhenSignatureDoesNotMatch(): void
    {
        $validator = new RequestSigningValidator('some-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(4));

        $request = new Request([], [], [], [], [], [
            'HTTP_X-Slack-Signature' => 'invalid-secret',
            'HTTP_X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $this->assertFalse($validator->validate($request));
    }

    public function testItInvalidatesRequestWhenKeyInvalid(): void
    {
        $validator = new RequestSigningValidator('invalid-secret');

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(4));

        $request = new Request([], [], [], [], [], [
            'HTTP_X-Slack-Signature' => 'v0=f928666dc3e47e1d919cf3639a171bc8d6e72028bddc393544ae80c81fcc4817',
            'HTTP_X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $this->assertFalse($validator->validate($request));
    }
}
