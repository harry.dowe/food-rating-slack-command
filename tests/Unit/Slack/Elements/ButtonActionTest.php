<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Elements;

use App\Slack\Elements\ButtonAction;
use App\Slack\Elements\SlackElement;
use Tests\TestCase;

class ButtonActionTest extends TestCase
{
    public function testItImplementsSlackElement(): void
    {
        $this->assertInstanceOf(SlackElement::class, new ButtonAction('', ''));
    }

    public function testItTransformsToArray(): void
    {
        $button = new ButtonAction('hello', 'some-id');

        $this->assertSame([
            'type' => 'button',
            'text' => [
                'type' => 'plain_text',
                'text' => 'hello',
            ],
            'action_id' => 'some-id',
        ], $button->toArray());
    }
}
