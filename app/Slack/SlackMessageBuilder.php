<?php declare(strict_types=1);

namespace App\Slack;

use App\FoodHygiene\EstablishmentsRepository;
use App\FoodHygiene\RepositoryException;
use App\Slack\Messages\SlackMessage;
use Psr\Log\LoggerInterface;

class SlackMessageBuilder
{
    /**
     * @var EstablishmentsRepository
     */
    private $establishments;

    /**
     * @var array
     */
    private $locations;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        EstablishmentsRepository $establishments,
        array $locations,
        LoggerInterface $logger
    ) {
        $this->establishments = $establishments;
        $this->logger = $logger;
        $this->locations = $locations;
    }

    public function build(string $name, string $location): SlackMessage
    {
        try {
            $establishments = $this->establishments->findAllByCoordinates(
                $name,
                $this->getLatitude($location),
                $this->getLongitude($location)
            );
        } catch (RepositoryException $exception) {
            $this->logger->error('Error connecting to gov.uk API.', [
                'exception' => $exception,
            ]);

            return new SlackMessage('Looks like something went wrong!');
        } catch (\InvalidArgumentException $exception) {
            $this->logger->error("Location data not set for `$location`", [
                'exception' => $exception,
            ]);

            return new SlackMessage($exception->getMessage());
        }

        if (empty($establishments)) {
            return new SlackMessage('Couldn\'t find any establishments.');
        }

        return new SlackMessage(
            sprintf('We found %d establishments.', count($establishments)),
            EstablishmentsFormatter::format($establishments),
        );
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function getLatitude(string $location): float
    {
        if (!isset($this->locations[$location]['latitude'])) {
            throw new \InvalidArgumentException("Could not find latitude for location `{$location}`");
        }

        return $this->locations[$location]['latitude'];
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function getLongitude(string $location): float
    {
        if (!isset($this->locations[$location]['longitude'])) {
            throw new \InvalidArgumentException('Could not find longitude for location: ' . $location);
        }

        return $this->locations[$location]['longitude'];
    }
}
