<?php declare(strict_types=1);

namespace Tests\Unit\Slack;

use App\Slack\ResponseType;
use PHPUnit\Framework\TestCase;

class ResponseTypeTest extends TestCase
{
    public function testItHasResponseTypeConstants(): void
    {
        $this->assertSame('ephemeral', ResponseType::EPHEMERAL);
        $this->assertSame('in_channel', ResponseType::IN_CHANNEL);
    }
}
