<?php declare(strict_types=1);

use App\Http\Middleware\SigningSecretMiddleware;

/* @var \Laravel\Lumen\Routing\Router $router */

$router->get('/', 'HomePageController@index');

$router->group(['middleware' => SigningSecretMiddleware::class], function () use ($router) {
    $router->post('commands/food-rating', 'HygieneRatingController@getRating');
    $router->post('actions', 'ActionsController@handleAction');
});
