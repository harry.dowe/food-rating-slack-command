<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Messages;

use App\Slack\Block\SlackBlock;
use App\Slack\Messages\SlackMessage;
use Tests\TestCase;

class SlackMessageTest extends TestCase
{
    public function testTurnsBlocksToArray(): void
    {
        $firstBlock = new class() implements SlackBlock {
            public function toArray(): array
            {
                return [
                    'baa' => 'sheep',
                ];
            }
        };

        $secondBlock = new class() implements SlackBlock {
            public function toArray(): array
            {
                return [
                    'moo' => 'cow',
                ];
            }
        };

        $message = new SlackMessage('hello');

        $message->setBlocks([
            $firstBlock,
            $secondBlock,
        ]);

        $this->assertSame([
            'response_type' => 'ephemeral',
            'text' => 'hello',
            'blocks' => [
                [
                    'baa' => 'sheep',
                ],
                [
                    'moo' => 'cow',
                ],
            ],
        ], $message->toArray());
    }

    public function testDisablesMarkdownUsingConstructorArgument(): void
    {
        $message = new SlackMessage('hello', [], false);

        $array = $message->toArray();

        $this->assertArrayHasKey('mrkdwn', $array);
        $this->assertSame(false, $array['mrkdwn']);
    }

    public function testDisablesMarkdownUsingSetter(): void
    {
        $message = new SlackMessage('hello');

        $message->withoutMarkdown();

        $array = $message->toArray();

        $this->assertArrayHasKey('mrkdwn', $array);
        $this->assertSame(false, $array['mrkdwn']);
    }

    public function testReturnsText(): void
    {
        $message = new SlackMessage('hello');

        $this->assertSame([
            'response_type' => 'ephemeral',
            'text' => 'hello',
        ], $message->toArray());
    }
}
