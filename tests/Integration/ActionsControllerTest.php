<?php declare(strict_types=1);

namespace Tests\Integration;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;
use Tests\Gateway\MockserverGateway;
use Tests\Gateway\Request;
use Tests\Gateway\Response;
use Tests\TestCase;

class ActionsControllerTest extends TestCase
{
    /**
     * @var MockserverGateway
     */
    private $mockserver;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->mockserver = new MockserverGateway(new Client([
            'base_uri' => 'mockserver:1080/mockserver/',
        ]));
    }

    public function setUp(): void
    {
        parent::setUp();

        Redis::flushdb();
    }

    /**
     * @before
     */
    public function resetMockserver(): void
    {
        $this->mockserver->reset();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testItSendsEstablishmentsToResponseUrl(): void
    {
        Redis::set('food-command:U0CA5', 'KFC');

        $establishmentsRequest = (new Request())
            ->setPath('/food-api/Establishments')
            ->setMethod('GET')
            ->setHeaders([
                'X-Api-Version' => '2',
                'Accept' => 'application/json',
            ])
            ->setQueryStringParameters([
                'name' => 'KFC',
                'latitude' => 53.5018821,
                'longitude' => -2.1967631,
            ]);

        $establishmentsResponse = (new Response())
            ->setStatusCode(200)
            ->setHeaders([
                'Content-Type' => 'application/json; charset=utf-8',
            ])
            ->setBody([
                'establishments' => [
                    [
                        'BusinessName' => 'KFC',
                        'FHRSID' => 334,
                        'RatingValue' => 5,
                    ],
                ],
            ]);

        $this->mockserver->createExpectation($establishmentsRequest, $establishmentsResponse);

        $slackRequest = (new Request())
            ->setPath('/slack/actions/T123567/1509734234')
            ->setMethod('POST')
            ->setJson([
                'response_type' => 'ephemeral',
                'text' => 'We found 1 establishments.',
                'blocks' => [
                    [
                        'type' => 'section',
                        'text' => [
                            'type' => 'mrkdwn',
                            'text' => '<https://ratings.food.gov.uk/business/334|KFC> has a rating of 5.',
                        ],
                    ],
                ],
            ]);

        $slackResponse = (new Response())
            ->setStatusCode(200);

        $this->mockserver->createExpectation($slackRequest, $slackResponse);

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818));

        $response = $this->post('actions', [
            'payload' => file_get_contents(base_path('tests/Mock/slackActionRequest.json')),
        ], [
            'X-Slack-Signature' => 'v0=ab5c9b13b0c0e2e36cc52c06994d5dd6c678b4967057b039cf750c501bedbd90',
            'x-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(200);
        $this->assertEmpty($this->response->getContent());

        $this->mockserver->verifySequence([$establishmentsRequest, $slackRequest]);
    }

    public function testItSendsErrorRequestWhenTextDoesNotExist(): void
    {
        $slackRequest = (new Request())
            ->setPath('/slack/actions/T123567/1509734234')
            ->setMethod('POST')
            ->setJson([
                'response_type' => 'ephemeral',
                'text' => 'Oops, something went wrong there.',
            ]);

        $slackResponse = (new Response())
            ->setStatusCode(200);

        $this->mockserver->createExpectation($slackRequest, $slackResponse);

        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818));

        $response = $this->post('actions', [
            'payload' => file_get_contents(base_path('tests/Mock/slackActionRequest.json')),
        ], [
            'X-Slack-Signature' => 'v0=ab5c9b13b0c0e2e36cc52c06994d5dd6c678b4967057b039cf750c501bedbd90',
            'x-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(200);
        $this->assertEmpty($this->response->getContent());

        $this->mockserver->verify($slackRequest, 1);
    }
}
