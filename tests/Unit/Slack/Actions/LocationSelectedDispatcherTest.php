<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Actions;

use App\Slack\Actions\LocationSelectedDispatcher;
use App\Slack\Actions\SlackActionException;
use App\Slack\Messages\SlackMessage;
use App\Slack\SlackGateway;
use App\Slack\SlackGatewayException;
use App\Slack\SlackMessageBuilder;
use GuzzleHttp\Psr7\Response;
use Illuminate\Redis\Connections\Connection;
use Tests\TestCase;

class LocationSelectedDispatcherTest extends TestCase
{
    public function testItDispatchesMessage(): void
    {
        $redis = \Mockery::mock(Connection::class);
        $redis->shouldReceive('exists')->once()->with('food-command:U0CA5')->andReturn(1);
        $redis->shouldReceive('get')->once()->with('food-command:U0CA5')->andReturn('KFC');

        $message = new SlackMessage('KFC');
        $response = new Response();
        $slack = \Mockery::mock(SlackGateway::class);
        $slack->shouldReceive('sendMessage')
            ->once()
            ->with($message, 'http://response-url.com')
            ->andReturn($response);

        $builder = \Mockery::mock(SlackMessageBuilder::class);
        $builder->shouldReceive('build')->once()->with('KFC', 'manchester')->andReturn($message);

        $dispatcher = new LocationSelectedDispatcher($redis, $slack, $builder);

        $response = $dispatcher->dispatch('U0CA5', 'manchester', 'http://response-url.com');
        $this->assertSame($response, $response);
    }

    public function testItThrowsSlackActionExceptionWhenUserInputDoesNotExist(): void
    {
        $redis = \Mockery::mock(Connection::class);
        $redis->shouldReceive('exists')->once()->with('food-command:U0CA5')->andReturn(0);

        $slack = \Mockery::mock(SlackGateway::class);
        $slack->shouldReceive('sendMessage')
            ->once()
            ->andReturn(new Response());

        $builder = \Mockery::mock(SlackMessageBuilder::class);

        $dispatcher = new LocationSelectedDispatcher($redis, $slack, $builder);

        $this->expectException(SlackActionException::class);
        $dispatcher->dispatch('U0CA5', 'manchester', 'http://response-url.com');
    }

    public function testItThrowsSlackActionExceptionWhenSlackGatewayErrorsWhenUserInputDoesNotExist(): void
    {
        $redis = \Mockery::mock(Connection::class);
        $redis->shouldReceive('exists')->once()->with('food-command:U0CA5')->andReturn(0);

        $slack = \Mockery::mock(SlackGateway::class);
        $slack->shouldReceive('sendMessage')->andThrow(SlackGatewayException::class);

        $builder = \Mockery::mock(SlackMessageBuilder::class);

        $dispatcher = new LocationSelectedDispatcher($redis, $slack, $builder);

        $this->expectException(SlackGatewayException::class);
        $dispatcher->dispatch('U0CA5', 'manchester', 'http://response-url.com');
    }

    public function testItThrowsSlackActionExceptionWhenSlackGatewayErrorsWhenUserInputExists(): void
    {
        $redis = \Mockery::mock(Connection::class);
        $redis->shouldReceive('exists')->once()->with('food-command:U0CA5')->andReturn(1);
        $redis->shouldReceive('get')->once()->with('food-command:U0CA5')->andReturn('KFC');

        $slack = \Mockery::mock(SlackGateway::class);
        $slack->shouldReceive('sendMessage')->andThrow(SlackGatewayException::class);

        $builder = \Mockery::mock(SlackMessageBuilder::class);

        $builder->shouldReceive('build')->withAnyArgs()->andReturn(new SlackMessage('message'));

        $dispatcher = new LocationSelectedDispatcher($redis, $slack, $builder);

        $this->expectException(SlackGatewayException::class);
        $dispatcher->dispatch('U0CA5', 'manchester', 'http://response-url.com');
    }
}
