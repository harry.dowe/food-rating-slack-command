<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Slack\Blocks\ActionsBlock;
use App\Slack\Elements\ButtonAction;
use App\Slack\Messages\SlackMessage;
use App\Slack\SlackMessageBuilder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Redis\Connections\Connection;
use Illuminate\Validation\ValidationException;

class HygieneRatingController extends Controller
{
    /**
     * @var SlackMessageBuilder
     */
    private $builder;

    /**
     * @var Connection
     */
    private $redis;

    public function __construct(SlackMessageBuilder $builder, Connection $redis)
    {
        $this->builder = $builder;
        $this->redis = $redis;
    }

    /**
     * @throws ValidationException
     */
    public function getRating(Request $request): JsonResponse
    {
        $this->validate($request, [
            'user_id' => 'required',
        ]);

        if (!$request->post('text')) {
            return new JsonResponse((new SlackMessage('You need to type the name of a restaurant.'))->toArray());
        }

        $this->redis->set('food-command:' . $request->post('user_id'), $request->post('text'));

        $message = new SlackMessage('Which office are you located at?', [
            new ActionsBlock([
                new ButtonAction('Manchester', 'manchester'),
                new ButtonAction('Pembrokeshire', 'pembrokeshire'),
                new ButtonAction('Swansea', 'swansea'),
                new ButtonAction('Liverpool', 'liverpool'),
            ]),
        ]);

        return new JsonResponse($message->toArray());
    }
}
