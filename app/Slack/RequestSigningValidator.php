<?php declare(strict_types=1);

namespace App\Slack;

use Carbon\Carbon;
use Illuminate\Http\Request;

class RequestSigningValidator
{
    private const SLACK_VERSION = 'v0';

    /**
     * @var string
     */
    private $signingSecret;

    public function __construct(string $signingSecret)
    {
        $this->signingSecret = $signingSecret;
    }

    public function validate(Request $request): bool
    {
        if (!$this->validateHeaders($request)) {
            return false;
        }

        $signature = SignatureBuilder::build(
            self::SLACK_VERSION,
            (int)$request->header('X-Slack-Request-Timestamp'),
            $request->input(),
            $this->signingSecret
        );

        return $this->signaturesMatch($request, $signature);
    }

    private function validateHeaders(Request $request): bool
    {
        if (!$request->hasHeader('X-Slack-Request-Timestamp')) {
            return false;
        }

        if (!$request->hasHeader('X-Slack-Signature')) {
            return false;
        }

        return Carbon::now()->getTimestamp() - (int)$request->header('X-Slack-Request-Timestamp') < 60 * 5;
    }

    private function signaturesMatch(Request $request, string $signature): bool
    {
        return hash_equals($request->header('X-Slack-Signature'), $signature);
    }
}
