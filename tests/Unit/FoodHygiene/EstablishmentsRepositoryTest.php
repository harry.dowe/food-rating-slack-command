<?php declare(strict_types=1);

namespace Tests\Unit\FoodHygiene;

use App\FoodHygiene\EstablishmentsRepository;
use App\FoodHygiene\RepositoryException;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class EstablishmentsRepositoryTest extends TestCase
{
    public function testItReturnsEstablishmentsByCoordinates(): void
    {
        $mock = new MockHandler([
            new Response(200, [], '{"establishments":[{"FHRSID":34,"BusinessName":"KFC","RatingValue":"5"}]}'),
        ]);
        $handler = HandlerStack::create($mock);

        $client = \Mockery::spy(Client::class, [
            ['handler' => $handler],
        ])->makePartial();

        $repository = new EstablishmentsRepository($client);

        $establishments = $repository->findAllByCoordinates('KFC', 53.5018821, -2.1967631);

        $this->assertCount(1, $establishments);
        $this->assertSame('KFC', $establishments[0]->getName());
        $this->assertSame(5, $establishments[0]->getRating());
        $this->assertSame('https://ratings.food.gov.uk/business/34', $establishments[0]->getUrl());

        $client->shouldHaveReceived('request')->withArgs(['GET', 'Establishments', [
            'query' => [
                'name' => 'KFC',
                'latitude' => 53.5018821,
                'longitude' => -2.1967631,
            ],
        ]]);
    }

    public function testItErrorsWhenBadResponse(): void
    {
        $mock = new MockHandler([
            new Response(500),
        ]);

        $client = new Client([
            'handler' => HandlerStack::create($mock),
        ]);

        $repository = new EstablishmentsRepository($client);

        $this->expectException(RepositoryException::class);

        $repository->findAllByCoordinates('KFC', 53.5018821, -2.1967631);
    }

    public function testItErrorsWithInvalidJsonResponse()
    {
        $mock = new MockHandler([
            new Response(200, [], 'dodgy json'),
        ]);

        $client = new Client([
            'handler' => HandlerStack::create($mock),
        ]);

        $repository = new EstablishmentsRepository($client);

        $this->expectException(RepositoryException::class);
        $repository->findAllByCoordinates('KFC', 53.5018821, -2.1967631);
    }
}
