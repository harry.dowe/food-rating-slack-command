<?php declare(strict_types=1);

namespace App\Slack;

class SignatureBuilder
{
    public static function build(string $version, int $timestamp, array $input, string $secret): string
    {
        $baseString = static::buildBaseString($version, $timestamp, $input);

        return sprintf('%s=%s', $version, static::hashBaseString($baseString, $secret));
    }

    private static function buildBaseString(string $version, int $timestamp, array $input): string
    {
        return sprintf('%s:%d:%s', $version, $timestamp, http_build_query($input));
    }

    private static function hashBaseString(string $baseString, string $secret): string
    {
        return hash_hmac('sha256', $baseString, $secret);
    }
}
