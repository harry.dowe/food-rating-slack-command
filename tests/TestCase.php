<?php declare(strict_types=1);

namespace Tests;

abstract class TestCase extends \Tests\Lumen\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
