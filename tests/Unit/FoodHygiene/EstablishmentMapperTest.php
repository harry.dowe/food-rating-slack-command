<?php declare(strict_types=1);

namespace Tests\Unit\FoodHygiene;

use App\FoodHygiene\EstablishmentMapper;
use Tests\TestCase;

class EstablishmentMapperTest extends TestCase
{
    public function testItMaps(): void
    {
        $establishment = EstablishmentMapper::map([
            'BusinessName' => 'The Greatest Business',
            'FHRSID' => 34,
            'RatingValue' => '5',
        ]);

        $this->assertSame('The Greatest Business', $establishment->getName());
        $this->assertSame(5, $establishment->getRating());
        $this->assertSame('https://ratings.food.gov.uk/business/34', $establishment->getUrl());
    }
}
