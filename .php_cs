<?php

$finder = PhpCsFixer\Finder::create()
    ->in('app')
    ->in('tests')
    ->in('bootstrap')
    ->in('config')
    ->in('public')
    ->in('routes')
    ->exclude('vendor');

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR2' => true,
        'declare_strict_types' => true,
        'declare_equal_normalize' => true,
        'array_syntax' => ['syntax' => 'short'],
        'binary_operator_spaces' => true,
        'cast_spaces' => ['space' => 'none'],
        'class_attributes_separation' => true,
        'combine_consecutive_unsets' => true,
        'concat_space' => ['spacing' => 'one'],
        'no_blank_lines_after_class_opening' => true,
        'no_blank_lines_after_phpdoc' => true,
        'no_extra_consecutive_blank_lines' => true,
        'no_trailing_comma_in_singleline_array' => true,
        'no_whitespace_in_blank_line' => true,
        'no_spaces_around_offset' => true,
        'no_unused_imports' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'no_whitespace_before_comma_in_array' => true,
        'normalize_index_brace' => true,
        'phpdoc_indent' => true,
        'phpdoc_to_comment' => true,
        'phpdoc_trim' => true,
        'single_quote' => true,
        'ternary_to_null_coalescing' => true,
        'trailing_comma_in_multiline_array' => true,
        'trim_array_spaces' => true,
        'method_argument_space' => ['ensure_fully_multiline' => false],
        'no_break_comment' => false,
        'blank_line_before_statement' => true,
        'return_assignment' => true,
        'phpdoc_scalar' => true,
        'phpdoc_no_empty_return' => true,
        'phpdoc_add_missing_param_annotation' => true,
        'php_unit_test_case_static_method_calls' => ['call_type' => 'this'],
        'php_unit_test_annotation' => true,
        'php_unit_method_casing' => true,
        'object_operator_without_whitespace' => true,
        'no_unneeded_control_parentheses' => true,
        'no_superfluous_elseif' => true,
        'no_multiline_whitespace_around_double_arrow' => true,
        'no_leading_namespace_whitespace' => true,
        'no_leading_import_slash' => true,
        'no_empty_statement' => true,
        'no_empty_phpdoc' => true,
        'no_superfluous_phpdoc_tags' => ['allow_mixed' => true],
        'multiline_whitespace_before_semicolons' => true,
        'method_chaining_indentation' => true,
        'lowercase_static_reference' => true,
        'lowercase_cast' => true,
        'list_syntax' => ['syntax' => 'short'],
        'heredoc_indentation' => true,
        'array_indentation' => true,
        'strict_comparison' => true,
        'space_after_semicolon' => true,
        'single_blank_line_before_namespace' => true,
        'return_type_declaration' => true,
        'no_extra_blank_lines' => [
            'tokens' => ['break', 'continue', 'curly_brace_block', 'return', 'throw', 'use', 'use_trait'],
        ],
    ])
    ->setFinder($finder);
