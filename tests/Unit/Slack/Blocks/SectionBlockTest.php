<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Blocks\SectionBlock;
use App\Slack\Elements\SlackElement;
use App\Slack\Messages\TextObject;
use Tests\TestCase;

class SectionBlockTest extends TestCase
{
    public function testItImplementsSlackBlockInterface(): void
    {
        $this->assertInstanceOf(SlackBlock::class, new SectionBlock(new TextObject('')));
    }

    public function testItTransformsToArray(): void
    {
        $section = new SectionBlock(new TextObject('hello'));

        $this->assertSame([
            'type' => 'section',
            'text' => [
                'type' => 'mrkdwn',
                'text' => 'hello',
            ],
        ], $section->toArray());
    }

    public function testItTransformsFields(): void
    {
        $section = new SectionBlock(new TextObject('hello'));

        $section->setFields([
            new TextObject('moo'),
            new TextObject('cow', 'plain_text'),
        ]);

        $this->assertSame([
            'type' => 'section',
            'text' => [
                'type' => 'mrkdwn',
                'text' => 'hello',
            ],
            'fields' => [
                [
                    'type' => 'mrkdwn',
                    'text' => 'moo',
                ],
                [
                    'type' => 'plain_text',
                    'text' => 'cow',
                ],
            ],
        ], $section->toArray());
    }

    public function testItTransformsAccessory(): void
    {
        $accessory = new class() implements SlackElement {
            public function toArray(): array
            {
                return [
                    'moo' => 'cow',
                ];
            }
        };

        $section = new SectionBlock(new TextObject('hello'));

        $section->setAccessory($accessory);

        $this->assertSame([
            'type' => 'section',
            'text' => [
                'type' => 'mrkdwn',
                'text' => 'hello',
            ],
            'accessory' => [
                'moo' => 'cow',
            ],
        ], $section->toArray());
    }
}
