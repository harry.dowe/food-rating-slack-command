build:
	docker-compose build

heroku-build:
	git push heroku master

serv:
	docker-compose up -d

down:
	docker-compose down

test:
	docker-compose run phpunit tests -c phpunit.xml

composer-install:
	docker-compose run composer install --no-interaction

composer-update:
	docker-compose run composer update --no-interaction

fmt:
	vendor/bin/php-cs-fixer fix

fmt-lint:
	vendor/bin/php-cs-fixer fix --dry-run --stop-on-violation
