<?php declare(strict_types=1);

namespace App\Slack\Elements;

use App\Slack\Messages\TextObject;

class ButtonAction implements SlackElement
{
    private const TYPE = 'button';

    /**
     * @var TextObject
     */
    private $text;

    /**
     * @var string
     */
    private $actionId;

    public function __construct(string $text, string $actionId)
    {
        $this->text = new TextObject($text, TextObject::PLAIN_TEXT);
        $this->actionId = $actionId;
    }

    public function toArray(): array
    {
        return [
            'type' => self::TYPE,
            'text' => $this->text->toArray(),
            'action_id' => $this->actionId,
        ];
    }
}
