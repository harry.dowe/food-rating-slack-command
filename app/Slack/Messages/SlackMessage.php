<?php declare(strict_types=1);

namespace App\Slack\Messages;

use App\Slack\Block\SlackBlock;
use App\Slack\ResponseType;

class SlackMessage
{
    /**
     * @var SlackBlock[]
     */
    private $blocks;

    /**
     * @var bool
     */
    private $markdown;

    /**
     * @var string
     */
    private $text;

    /**
     * @param SlackBlock[] $blocks
     */
    public function __construct(string $text, array $blocks = [], bool $markdown = true)
    {
        $this->text = $text;
        $this->markdown = $markdown;
        $this->blocks = $blocks;
    }

    /**
     * @param SlackBlock[] $blocks
     */
    public function setBlocks(array $blocks): self
    {
        $this->blocks = $blocks;

        return $this;
    }

    private function hasBlocks(): bool
    {
        return !empty($this->blocks);
    }

    public function withoutMarkdown()
    {
        $this->markdown = false;
    }

    public function toArray(): array
    {
        $message = [
            'response_type' => ResponseType::EPHEMERAL,
            'text' => $this->text,
        ];

        if ($this->hasBlocks()) {
            $message['blocks'] = array_map(function (SlackBlock $block): array {
                return $block->toArray();
            }, $this->blocks);
        }

        if (!$this->markdown) {
            $message['mrkdwn'] = false;
        }

        return $message;
    }
}
