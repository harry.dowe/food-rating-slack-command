<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Elements;

use App\Slack\Elements\ButtonLink;
use App\Slack\Elements\SlackElement;
use App\Slack\Messages\TextObject;
use Tests\TestCase;

class ButtonLinkTest extends TestCase
{
    public function testItImplementsSlackElementInterface(): void
    {
        $this->assertInstanceOf(SlackElement::class, new ButtonLink('', ''));
    }

    public function testItTransformsToArrayUsingTextObject(): void
    {
        $buttonLink = new ButtonLink(new TextObject('Click me'), 'http://example.com');

        $this->assertSame([
            'type' => 'button',
            'url' => 'http://example.com',
            'text' => [
                'type' => 'mrkdwn',
                'text' => 'Click me',
            ],
        ], $buttonLink->toArray());
    }

    public function testItTransformsToArrayUsingString(): void
    {
        $buttonLink = new ButtonLink('Click me', 'http://example.com');

        $this->assertSame([
            'type' => 'button',
            'url' => 'http://example.com',
            'text' => [
                'type' => 'plain_text',
                'text' => 'Click me',
            ],
        ], $buttonLink->toArray());
    }
}
