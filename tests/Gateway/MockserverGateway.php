<?php declare(strict_types=1);

namespace Tests\Gateway;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class MockserverGateway
{
    /**
     * @var ClientInterface
     */
    protected $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function reset(): ResponseInterface
    {
        return $this->client->request('PUT', '/reset');
    }

    public function createExpectation(Request $request, Response $response): ResponseInterface
    {
        return $this->client->request('PUT', 'expectation', [
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/json',
            ],
            RequestOptions::JSON => [
                [
                    'httpRequest' => $request->build(),
                    'httpResponse' => $response->build(),
                ],
            ],
        ]);
    }

    public function verifySequence(array $sequence): ResponseInterface
    {
        return $this->client->request('PUT', 'verifySequence', [
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/json',
            ],
            RequestOptions::JSON => [
                'httpRequests' => array_map(function (Request $request): array {
                    return $request->build();
                }, $sequence),
            ],
        ]);
    }

    public function verify(Request $request, int $times): ResponseInterface
    {
        return $this->client->request('PUT', 'verify', [
            RequestOptions::HEADERS => [
                'Content-Type' => 'application/json',
            ],
            RequestOptions::JSON => [
                'httpRequest' => $request->build(),
                'times' => [
                    'atLeast' => $times,
                    'atMost' => $times,
                ],
            ],
        ]);
    }
}
