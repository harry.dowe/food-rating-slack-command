<?php declare(strict_types=1);

namespace App\FoodHygiene;

class EstablishmentMapper
{
    public static function map(array $establishment): Establishment
    {
        return (new Establishment())
            ->setName($establishment['BusinessName'])
            ->setId($establishment['FHRSID'])
            ->setRating((int)$establishment['RatingValue']);
    }
}
