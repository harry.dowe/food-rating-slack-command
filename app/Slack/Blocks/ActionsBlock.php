<?php declare(strict_types=1);

namespace App\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Elements\SlackElement;

class ActionsBlock implements SlackBlock
{
    private const TYPE = 'actions';

    /**
     * @var SlackElement[]
     */
    private $elements;

    /**
     * @param SlackElement[] $elements
     */
    public function __construct(array $elements)
    {
        $this->elements = $elements;
    }

    public function toArray(): array
    {
        return [
            'type' => self::TYPE,
            'elements' => array_map(function (SlackElement $element): array {
                return $element->toArray();
            }, $this->elements),
        ];
    }
}
