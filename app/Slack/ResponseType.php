<?php declare(strict_types=1);

namespace App\Slack;

class ResponseType
{
    public const EPHEMERAL = 'ephemeral';

    public const IN_CHANNEL = 'in_channel';
}
