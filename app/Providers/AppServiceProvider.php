<?php declare(strict_types=1);

namespace App\Providers;

use App\FoodHygiene\EstablishmentsRepository;
use App\Slack\RequestSigningValidator;
use App\Slack\SlackGateway;
use App\Slack\SlackMessageBuilder;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Illuminate\Redis\Connections\Connection;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->when(EstablishmentsRepository::class)
            ->needs(ClientInterface::class)
            ->give(function ($app): ClientInterface {
                return new Client([
                    'base_uri' => $app->config->get('app.food_api_url'),
                    RequestOptions::HEADERS => [
                        'X-Api-Version' => 2,
                        'Accept' => 'application/json',
                    ],
                ]);
            });

        $this->app->when(SlackGateway::class)
            ->needs(ClientInterface::class)
            ->give(function (): ClientInterface {
                return new Client([
                    RequestOptions::HEADERS => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],
                ]);
            });

        $this->app->when(RequestSigningValidator::class)
            ->needs('$signingSecret')
            ->give($this->app->config->get('app.slack_signing_secret'));

        $this->app->when(SlackMessageBuilder::class)
            ->needs('$locations')
            ->give($this->app->config->get('locations', []));

        $this->app->bind(Connection::class, function (Application $app): Connection {
            return $app['redis']->connection();
        });
    }
}
