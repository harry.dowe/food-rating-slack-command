<?php declare(strict_types=1);

namespace App\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Elements\SlackElement;
use App\Slack\Messages\TextObject;

class SectionBlock implements SlackBlock
{
    private const TYPE = 'section';

    /**
     * @var TextObject
     */
    private $text;

    /**
     * @var TextObject[]
     */
    private $fields;

    /**
     * @var SlackElement
     */
    protected $accessory;

    public function __construct(TextObject $text)
    {
        $this->text = $text;
    }

    /**
     * @param TextObject[] $fields
     */
    public function setFields(array $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    private function hasFields(): bool
    {
        return !empty($this->fields);
    }

    public function setAccessory(SlackElement $element): self
    {
        $this->accessory = $element;

        return $this;
    }

    private function hasAccessory(): bool
    {
        return isset($this->accessory);
    }

    public function toArray(): array
    {
        $block = [
            'type' => self::TYPE,
            'text' => $this->text->toArray(),
        ];

        if ($this->hasFields()) {
            $block['fields'] = array_map(function (TextObject $field): array {
                return $field->toArray();
            }, $this->fields);
        }

        if ($this->hasAccessory()) {
            $block['accessory'] = $this->accessory->toArray();
        }

        return $block;
    }
}
