<?php declare(strict_types=1);

namespace App\Slack\Messages;

class TextObject
{
    public const PLAIN_TEXT = 'plain_text';

    public const MARKDOWN = 'mrkdwn';

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $type;

    public function __construct(string $text, string $type = self::MARKDOWN)
    {
        if (!in_array($type, [self::PLAIN_TEXT, self::MARKDOWN])) {
            throw new \InvalidArgumentException("Unsupported text type `$type`.");
        }

        $this->text = $text;
        $this->type = $type;
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'text' => $this->text,
        ];
    }
}
