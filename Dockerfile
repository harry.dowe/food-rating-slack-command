FROM php:7.3.3-fpm

WORKDIR /var/www/html/

RUN pecl install xdebug-2.9.8 && docker-php-ext-enable xdebug

COPY app app
COPY bootstrap bootstrap
COPY public public
COPY resources resources
COPY routes routes
COPY storage storage
COPY vendor vendor

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

COPY --chown=www:www . /var/www/html

USER www

CMD ["php-fpm"]
