<?php declare(strict_types=1);

namespace App\FoodHygiene;

class Establishment
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $rating;

    /**
     * @var int
     */
    private $id;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): string
    {
        return 'https://ratings.food.gov.uk/business/' . (string)$this->id;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getRating(): int
    {
        return $this->rating;
    }
}
