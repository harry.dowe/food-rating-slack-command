<?php declare(strict_types=1);

namespace App\FoodHygiene;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class EstablishmentsRepository
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @throws RepositoryException
     */
    public function findAllByCoordinates(string $name, float $latitude, float $longitude): array
    {
        try {
            $response = $this->client->request('GET', 'Establishments', [
                RequestOptions::QUERY => [
                    'name' => $name,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                ],
            ]);
        } catch (GuzzleException $e) {
            throw new RepositoryException('Something went wrong when trying to communicate with the gov.uk API.', 0, $e);
        }

        try {
            $body = json_decode((string)$response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            throw new RepositoryException('Error decoding JSON response from gov.uk API: ' . $exception->getMessage(), 0, $exception);
        }

        return array_map([EstablishmentMapper::class, 'map'], $body['establishments']);
    }
}
