<?php declare(strict_types=1);

namespace App\Slack;

use App\FoodHygiene\Establishment;
use App\Slack\Blocks\DividerBlock;
use App\Slack\Blocks\SectionBlock;
use App\Slack\Messages\TextObject;

class EstablishmentsFormatter
{
    /**
     * @param Establishment[]
     *
     * @return DividerBlock[]|SectionBlock[]
     */
    public static function format(array $establishments): array
    {
        $divider = new DividerBlock();

        $array = [];
        foreach ($establishments as $establishment) {
            $array[] = self::makeBlock($establishment);
            $array[] = $divider;
        }

        array_pop($array);

        return $array;
    }

    private static function makeBlock(Establishment $establishment): SectionBlock
    {
        return new SectionBlock(new TextObject(
            sprintf('<%s|%s> has a rating of %s.',
                $establishment->getUrl(),
                $establishment->getName(),
                $establishment->getRating(),
            ),
        ));
    }
}
