<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Helpers\Json;
use App\Slack\Actions\LocationSelectedDispatcher;
use App\Slack\Actions\SlackActionException;
use App\Slack\SlackGatewayException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Psr\Log\LoggerInterface;

class ActionsController extends Controller
{
    /**
     * @var LocationSelectedDispatcher
     */
    protected $dispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LocationSelectedDispatcher $dispatcher, LoggerInterface $logger)
    {
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    public function handleAction(Request $request): Response
    {
        $this->validate($request, [
            'payload' => 'required|json',
        ]);

        $payload = Json::decode($request->post('payload'));

        try {
            $this->dispatcher->dispatch(
                $payload['user']['id'],
                Arr::first($payload['actions'])['action_id'],
                $payload['response_url'],
            );
        } catch (SlackActionException | SlackGatewayException $exception) {
            $this->logger->error('Error occurred when dispatching action.', [
                'exception' => (string)$exception,
            ]);
        }

        return new Response();
    }
}
