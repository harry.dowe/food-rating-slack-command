<?php declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request         $request
     *
     * @param Exception|HttpExceptionInterface $exception
     *
     * @return Response
     */
    public function render($request, Exception $exception)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof HttpResponseException) {
            return $exception->getResponse();
        }
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception);
        }

        return new JsonResponse($this->convertExceptionToArray($exception),
            $this->isHttpException($exception) ? $exception->getStatusCode() : 500,
            $this->isHttpException($exception) ? $exception->getHeaders() : [],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    /**
     * @return Exception|NotFoundHttpException|AccessDeniedHttpException|HttpException
     */
    protected function prepareException(Exception $exception): Exception
    {
        if ($exception instanceof ModelNotFoundException) {
            $exception = new NotFoundHttpException($exception->getMessage(), $exception);
        } elseif ($exception instanceof AuthorizationException) {
            $exception = new AccessDeniedHttpException($exception->getMessage(), $exception);
        } elseif ($exception instanceof TokenMismatchException) {
            $exception = new HttpException(419, $exception->getMessage(), $exception);
        }

        return $exception;
    }

    protected function convertValidationExceptionToResponse(ValidationException $exception): Response
    {
        if ($exception->response) {
            return $exception->response;
        }

        return new JsonResponse([
            'message' => $exception->getMessage(),
            'errors' => $exception->errors(),
        ], $exception->status);
    }

    protected function convertExceptionToArray(Exception $exception): array
    {
        if (config('app.debug')) {
            return [
                'message' => $exception->getMessage(),
                'exception' => get_class($exception),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => collect($exception->getTrace())->map(function (array $trace): array {
                    return Arr::except($trace, ['args']);
                })->all(),
            ];
        }

        return [
            'message' => $this->isHttpException($exception) ? $exception->getMessage() : 'Server Error',
        ];
    }

    protected function isHttpException(Exception $exception): bool
    {
        return $exception instanceof HttpExceptionInterface;
    }
}
