<?php declare(strict_types=1);

namespace App\Helpers;

class Json
{
    /**
     * @throws \JsonException
     */
    public static function decode(string $json, int $depth = 512, int $options = 0): array
    {
        return json_decode($json, true, $depth, JSON_THROW_ON_ERROR | $options);
    }
}
