<?php declare(strict_types=1);

namespace Tests\Integration;

use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class HygieneRatingControllerTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Redis::flushdb();
    }

    public function testItReturnsButtonsForLocationSelectionAndStoresTextInRedis(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818));

        $response = $this->post('commands/food-rating', [
            'user_id' => 'U2147483697',
            'text' => 'hello',
        ], [
            'X-Slack-Signature' => 'v0=8142e3ebd0b2b11215d90351b8c6ecd3262d16863ce211383bccdf6a6e58f7d6',
            'x-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(200);

        $this->assertJsonStringEqualsJsonString(
            file_get_contents(base_path('tests/Mock/slackFoodRatingCommandResponse.json')),
            $this->response->getContent()
        );

        $this->assertSame(1, Redis::exists('food-command:U2147483697'));
        $this->assertSame('hello', Redis::get('food-command:U2147483697'));
    }

    public function testItBlocksInvalidSignature(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(1));

        $response = $this->post('commands/food-rating', [
            'text' => 'hello',
        ], [
            'X-Slack-Signature' => 'invalid-signature',
            'X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(400);
    }

    public function testItReturnsUnprocessableEntityWithMissingRequiredField(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818)->addMinutes(1));

        $response = $this->post('commands/food-rating', [
            'not' => 'text',
        ], [
            'X-Slack-Signature' => 'v0=68cd26d13c0fef7970f7c31c077bf4a9a35835a7c807d00938565e46c8b24259',
            'X-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(422);

        $this->assertJsonStringEqualsJsonString(<<<EOT
            {
                "user_id": [
                    "The user id field is required."
                ]
            }
            EOT, $this->response->getContent());
    }

    public function testItReturnsMissingRestaurantWhenTextIsMissing(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818));

        $response = $this->post('commands/food-rating', [
            'user_id' => 'U2147483697',
        ], [
            'X-Slack-Signature' => 'v0=a52f61c211f4ef1e85d27c85a63c81dbdccefaf70909cb1db4ae8b1e30d8505f',
            'x-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(200);

        $this->assertJsonStringEqualsJsonString(<<<EOT
            {
                "response_type": "ephemeral",
                "text": "You need to type the name of a restaurant."
            }
            EOT, $this->response->getContent()
        );
    }

    public function testItReturnsMissingRestaurantWhenTextIsEmpty(): void
    {
        Carbon::setTestNow(Carbon::createFromTimestamp(1553249818));

        $response = $this->post('commands/food-rating', [
            'user_id' => 'U2147483697',
            'text' => '',
        ], [
            'X-Slack-Signature' => 'v0=b460c3ce6773ec137fb2efab962bced8491020ad419526956dc5ebab45ce8197',
            'x-Slack-Request-Timestamp' => 1553249818,
        ]);

        $response->assertResponseStatus(200);

        $this->assertJsonStringEqualsJsonString(
            <<<EOT
                {
                    "response_type": "ephemeral",
                    "text": "You need to type the name of a restaurant."
                }
                EOT, $this->response->getContent()
        );
    }
}
