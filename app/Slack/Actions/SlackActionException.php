<?php declare(strict_types=1);

namespace App\Slack\Actions;

class SlackActionException extends \RuntimeException
{
}
