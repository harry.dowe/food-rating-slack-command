<?php declare(strict_types=1);

namespace App\Slack\Elements;

use App\Slack\Messages\TextObject;

class ButtonLink implements SlackElement
{
    private const TYPE = 'button';

    /**
     * @var TextObject
     */
    private $text;

    /**
     * @var string
     */
    private $url;

    /**
     * ButtonLink constructor.
     *
     * @param TextObject|string $text
     */
    public function __construct($text, string $url)
    {
        if (is_string($text)) {
            $text = new TextObject($text, TextObject::PLAIN_TEXT);
        }

        $this->text = $text;
        $this->url = $url;
    }

    public function toArray(): array
    {
        return [
            'type' => self::TYPE,
            'url' => $this->url,
            'text' => $this->text->toArray(),
        ];
    }
}
