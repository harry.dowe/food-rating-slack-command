<?php declare(strict_types=1);

namespace App\Http\Middleware;

use App\Slack\RequestSigningValidator;
use Closure;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SigningSecretMiddleware
{
    /**
     * @var RequestSigningValidator
     */
    private $validator;

    public function __construct(RequestSigningValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     *
     * @throws BadRequestHttpException
     */
    public function handle($request, Closure $next)
    {
        if (!$this->validator->validate($request)) {
            throw new BadRequestHttpException('The request could not be processed.');
        }

        return $next($request);
    }
}
