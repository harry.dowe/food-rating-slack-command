<?php declare(strict_types=1);

namespace Tests\Gateway;

class Response
{
    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var string
     */
    private $body;

    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    private function hasStatusCode(): bool
    {
        return isset($this->statusCode);
    }

    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    private function hasHeaders(): bool
    {
        return !empty($this->headers);
    }

    /**
     * @param mixed|string $body
     *
     * @return Response
     */
    public function setBody($body): self
    {
        if (is_string($this->body) || $body === null) {
            $this->body = $body;
        } else {
            $this->body = json_encode($body);
        }

        return $this;
    }

    private function hasBody(): bool
    {
        return isset($this->body);
    }

    public function build(): array
    {
        $response = [];

        if ($this->hasStatusCode()) {
            $response['statusCode'] = $this->statusCode;
        }

        if ($this->hasHeaders()) {
            $response['headers'] = MockserverHelper::mapKeyToMultiValue($this->headers);
        }

        if ($this->hasBody()) {
            $response['body'] = $this->body;
        }

        return $response;
    }
}
