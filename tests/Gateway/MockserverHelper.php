<?php declare(strict_types=1);

namespace Tests\Gateway;

use Illuminate\Support\Collection;

class MockserverHelper
{
    public static function mapKeyToMultiValue(array $headers): array
    {
        return (new Collection($headers))
            ->map(function (string $value, string $key): array {
                return [
                    'name' => $key,
                    'values' => [
                        $value,
                    ],
                ];
            })->values()->all();
    }
}
