<?php declare(strict_types=1);

return [
    'manchester' => [
        'latitude' => 53.5018821,
        'longitude' => -2.1967631,
    ],
    'lancashire' => [
        'latitude' => 53.706842,
        'longitude' => -2.33155,
    ],
    'liverpool' => [
        'latitude' => 53.4039591,
        'longitude' => -2.9822519,
    ],
    'pembrokeshire' => [
        'latitude' => 51.7009033,
        'longitude' => -4.928319,
    ],
];
