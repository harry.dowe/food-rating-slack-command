<?php declare(strict_types=1);

namespace App\Slack\Actions;

use App\Slack\Messages\SlackMessage;
use App\Slack\SlackGateway;
use App\Slack\SlackGatewayException;
use App\Slack\SlackMessageBuilder;
use Illuminate\Redis\Connections\Connection;
use Psr\Http\Message\ResponseInterface;

class LocationSelectedDispatcher
{
    /**
     * @var Connection
     */
    private $redis;

    /**
     * @var SlackGateway
     */
    private $slack;

    /**
     * @var SlackMessageBuilder
     */
    private $builder;

    public function __construct(Connection $redis, SlackGateway $slack, SlackMessageBuilder $builder)
    {
        $this->redis = $redis;
        $this->slack = $slack;
        $this->builder = $builder;
    }

    /**
     * @throws SlackActionException
     * @throws SlackGatewayException
     */
    public function dispatch(string $userId, string $location, string $responseUrl): ResponseInterface
    {
        if (!$this->hasText($userId)) {
            $this->slack->sendMessage(new SlackMessage('Oops, something went wrong there.'), $responseUrl);

            throw new SlackActionException('Could not find user input.');
        }

        $message = $this->builder->build(
            $this->getText($userId),
            $location,
        );

        return $this->slack->sendMessage($message, $responseUrl);
    }

    private function getText(string $userId): string
    {
        return $this->redis->get('food-command:' . $userId);
    }

    private function hasText(string $userId): bool
    {
        return $this->redis->exists('food-command:' . $userId) === 1;
    }
}
