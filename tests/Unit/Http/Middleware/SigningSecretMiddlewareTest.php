<?php declare(strict_types=1);

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\SigningSecretMiddleware;
use App\Slack\RequestSigningValidator;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SigningSecretMiddlewareTest extends TestCase
{
    public function testItAcceptsValidRequests(): void
    {
        $validator = \Mockery::mock(RequestSigningValidator::class);
        $validator->shouldReceive('validate')->once()->andReturn(true);

        $middleware = new SigningSecretMiddleware($validator);

        $request = new Request();

        $actual = $middleware->handle($request, function (Request $request): Request {
            return $request;
        });

        $this->assertSame($request, $actual);
    }

    public function testItThrowsBadRequestExceptionWhenRequestSigningFails(): void
    {
        $validator = \Mockery::mock(RequestSigningValidator::class);
        $validator->shouldReceive('validate')->once()->andReturn(false);

        $middleware = new SigningSecretMiddleware($validator);

        $request = new Request();

        $this->expectException(BadRequestHttpException::class);
        $middleware->handle($request, function () {
        });
    }
}
