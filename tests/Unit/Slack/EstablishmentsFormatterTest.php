<?php declare(strict_types=1);

namespace Tests\Unit\Slack;

use App\FoodHygiene\Establishment;
use App\Slack\Blocks\SectionBlock;
use App\Slack\EstablishmentsFormatter;
use App\Slack\Blocks\DividerBlock;
use Tests\TestCase;

class EstablishmentsFormatterTest extends TestCase
{
    public function testItInsertsDividerBlocks(): void
    {
        $blocks = EstablishmentsFormatter::format([
            (new Establishment())->setId(22)->setName('first-name')->setRating(4),
            (new Establishment())->setId(56)->setName('second-name')->setRating(3),
        ]);

        $this->assertCount(3, $blocks);

        [$first, $divider, $second] = $blocks;

        $this->assertInstanceOf(SectionBlock::class, $first);
        $this->assertInstanceOf(DividerBlock::class, $divider);
        $this->assertInstanceOf(SectionBlock::class, $second);
        $this->assertSame([
            'type' => 'section',
            'text' => [
                'type' => 'mrkdwn',
                'text' => '<https://ratings.food.gov.uk/business/22|first-name> has a rating of 4.',
            ],
        ], $first->toArray());

        $this->assertSame([
            'type' => 'section',
            'text' => [
                'type' => 'mrkdwn',
                'text' => '<https://ratings.food.gov.uk/business/56|second-name> has a rating of 3.',
            ],
        ], $second->toArray());
    }
}
