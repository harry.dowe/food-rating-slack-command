<?php declare(strict_types=1);

namespace App\Slack\Elements;

interface SlackElement
{
    public function toArray(): array;
}
