<?php declare(strict_types=1);

namespace Tests\Unit\FoodHygiene;

use App\FoodHygiene\Establishment;
use Tests\TestCase;

class EstablishmentTest extends TestCase
{
    public function testItReturnsUrl(): void
    {
        $establishment = (new Establishment())->setId(555);
        $this->assertSame('https://ratings.food.gov.uk/business/555', $establishment->getUrl());
    }
}
