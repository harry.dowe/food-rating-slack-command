<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Blocks\ImageBlock;
use App\Slack\Messages\TextObject;
use Tests\TestCase;

class ImageBlockTest extends TestCase
{
    public function testItImplementsSlackBlock(): void
    {
        $this->assertInstanceOf(SlackBlock::class, new ImageBlock('', ''));
    }

    public function testItTransformsToArray(): void
    {
        $image = new ImageBlock('example.com/image.png', 'Example image');

        $this->assertSame([
            'type' => 'image',
            'image_url' => 'example.com/image.png',
            'alt_text' => 'Example image',
        ], $image->toArray());
    }

    public function testItTransformsTitle(): void
    {
        $image = new ImageBlock('example.com/image.png', 'Example image');

        $image->setTitle(new TextObject('sheep'));

        $this->assertSame([
            'type' => 'image',
            'image_url' => 'example.com/image.png',
            'alt_text' => 'Example image',
            'title' => [
                'type' => 'mrkdwn',
                'text' => 'sheep',
            ],
        ], $image->toArray());
    }
}
