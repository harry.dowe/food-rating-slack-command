<?php declare(strict_types=1);

use Illuminate\Support\Str;

return [
    'debug' => env('APP_DEBUG', false),
    'timezone' => 'UTC',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'food_api_url' => Str::finish(env('FOOD_API_URL'), '/'),
    'slack_signing_secret' => env('SLACK_SIGNING_SECRET'),
];
