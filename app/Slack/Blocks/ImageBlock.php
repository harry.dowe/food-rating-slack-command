<?php declare(strict_types=1);

namespace App\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Messages\TextObject;

class ImageBlock implements SlackBlock
{
    private const TYPE = 'image';

    /**
     * @var string
     */
    private $imageUrl;

    /**
     * @var string
     */
    private $altText;

    /**
     * @var TextObject
     */
    private $title;

    public function __construct(string $imageUrl, string $altText)
    {
        $this->imageUrl = $imageUrl;
        $this->altText = $altText;
    }

    public function setTitle(TextObject $title): self
    {
        $this->title = $title;

        return $this;
    }

    private function hasTitle(): bool
    {
        return $this->title instanceof TextObject;
    }

    public function toArray(): array
    {
        $image = [
            'type' => self::TYPE,
            'image_url' => $this->imageUrl,
            'alt_text' => $this->altText,
        ];

        if ($this->hasTitle()) {
            $image['title'] = $this->title->toArray();
        }

        return $image;
    }
}
