<?php declare(strict_types=1);

namespace Tests\Unit\Slack\Blocks;

use App\Slack\Block\SlackBlock;
use App\Slack\Blocks\ActionsBlock;
use App\Slack\Elements\SlackElement;
use Tests\TestCase;

class ActionsBlockTest extends TestCase
{
    public function testItImplementsSlackBlock(): void
    {
        $this->assertInstanceOf(SlackBlock::class, new ActionsBlock([]));
    }

    public function testItTransformsToArray(): void
    {
        $action = new class() implements SlackElement {
            public function toArray(): array
            {
                return [
                    'baa' => 'sheep',
                ];
            }
        };

        $block = new ActionsBlock([
            $action,
        ]);

        $this->assertSame([
            'type' => 'actions',
            'elements' => [
                [
                    'baa' => 'sheep',
                ],
            ],
        ], $block->toArray());
    }
}
