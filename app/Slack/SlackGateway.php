<?php declare(strict_types=1);

namespace App\Slack;

use App\Slack\Messages\SlackMessage;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class SlackGateway
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @throw SlackGatewayException
     */
    public function sendMessage(SlackMessage $message, string $responseUrl): ResponseInterface
    {
        try {
            return $this->client->request('POST', $responseUrl, [
                RequestOptions::JSON => $message->toArray(),
            ]);
        } catch (GuzzleException $e) {
            throw new SlackGatewayException('Error when sending response to Slack: ' . $e->getMessage(), 0, $e);
        }
    }
}
