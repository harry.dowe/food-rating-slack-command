<?php declare(strict_types=1);

namespace App\Slack\Block;

interface SlackBlock
{
    public function toArray(): array;
}
