<?php

/** @noinspection PhpSuperClassIncompatibleWithInterfaceInspection */

declare(strict_types=1);

namespace Tests\Unit\Slack;

use App\Slack\Blocks\SectionBlock;
use App\Slack\Messages\SlackMessage;
use App\Slack\Messages\TextObject;
use App\Slack\SlackGateway;
use App\Slack\SlackGatewayException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class SlackGatewayTest extends TestCase
{
    public function testItSendsMessage(): void
    {
        $mock = new MockHandler([
            new Response(200),
        ]);
        $handler = HandlerStack::create($mock);

        $client = \Mockery::spy(Client::class, [
            ['handler' => $handler],
        ])->makePartial();

        $gateway = new SlackGateway($client);

        $response = $gateway->sendMessage(new SlackMessage('hello'), 'http://example.com');

        $this->assertSame(200, $response->getStatusCode());

        $client->shouldHaveReceived('request')->with('POST', 'http://example.com', [
            'json' => [
                'response_type' => 'ephemeral',
                'text' => 'hello',
            ],
        ]);
    }

    public function testItDisabledMarkdown(): void
    {
        $mock = new MockHandler([
            new Response(200),
        ]);
        $handler = HandlerStack::create($mock);

        $client = \Mockery::spy(Client::class, [
            ['handler' => $handler],
        ])->makePartial();

        $gateway = new SlackGateway($client);

        $message = new SlackMessage('hello');
        $message->withoutMarkdown();
        $response = $gateway->sendMessage($message, 'http://example.com');

        $this->assertSame(200, $response->getStatusCode());

        $client->shouldHaveReceived('request')->with('POST', 'http://example.com', [
            'json' => [
                'response_type' => 'ephemeral',
                'text' => 'hello',
                'mrkdwn' => false,
            ],
        ]);
    }

    public function testItSendMessageWithBlocks(): void
    {
        $mock = new MockHandler([
            new Response(200),
        ]);
        $handler = HandlerStack::create($mock);

        $client = \Mockery::spy(Client::class, [
            ['handler' => $handler],
        ])->makePartial();

        $gateway = new SlackGateway($client);

        $message = new SlackMessage('hello', [
            new SectionBlock(new TextObject('hello section')),
        ]);

        $response = $gateway->sendMessage($message, 'http://example.com');

        $this->assertSame(200, $response->getStatusCode());

        $client->shouldHaveReceived('request')->with('POST', 'http://example.com', [
            'json' => [
                'response_type' => 'ephemeral',
                'text' => 'hello',
                'blocks' => [
                    [
                        'type' => 'section',
                        'text' => [
                            'type' => 'mrkdwn',
                            'text' => 'hello section',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testItThrowsSlackGatewayExceptionWhenGuzzleErrors(): void
    {
        $mock = new MockHandler([
            new class() extends \Exception implements GuzzleException {
            },
        ]);
        $handler = HandlerStack::create($mock);

        $client = \Mockery::spy(Client::class, [
            ['handler' => $handler],
        ])->makePartial();

        $gateway = new SlackGateway($client);

        $this->expectException(SlackGatewayException::class);
        $gateway->sendMessage(new SlackMessage('hello'), 'http://example.com');
    }
}
