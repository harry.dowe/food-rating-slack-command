<?php declare(strict_types=1);

namespace Tests\Unit\Slack;

use App\Slack\SignatureBuilder;
use PHPUnit\Framework\TestCase;

class SignatureBuilderTest extends TestCase
{
    public function testItBuildsSignatures(): void
    {
        $actual = SignatureBuilder::build(
            'v0',
            1553249818,
            ['query' => 'value', 'king' => 'queen'],
            'some-secret'
        );

        $this->assertSame('v0=f928666dc3e47e1d919cf3639a171bc8d6e72028bddc393544ae80c81fcc4817', $actual);
    }

    public function testItBuildsSignaturesWithDifferentKeys(): void
    {
        $actual = SignatureBuilder::build(
            'v0',
            1553249818,
            ['query' => 'value', 'king' => 'queen'],
            'some-other-secret'
        );

        $this->assertSame('v0=97c79a818f196b8be3db0aa4cf8d2914c9b11912b082472eec894f5cf058dfe9', $actual);
    }

    public function testItBuildsSignaturesWithDifferentVersion(): void
    {
        $actual = SignatureBuilder::build(
            'v1',
            1553249818,
            ['query' => 'value', 'king' => 'queen'],
            'some-other-secret',
        );

        $this->assertSame('v1=578245ce79e3723a69396eef61871f28dfe6262e5c71599a1f8fab75d2293323', $actual);
    }

    public function testItBuildsSignaturesWithDifferentParams(): void
    {
        $actual = SignatureBuilder::build(
            'v0',
            1553249818,
            ['moo' => 'cow'],
            'some-other-secret'
        );

        $this->assertSame('v0=625e048e81b8e9b4faa82f62eb35ec42d88284bc99bd475592e5f0923ac10c4a', $actual);
    }

    public function testItBuildsSignaturesWithDifferentTimestamp(): void
    {
        $actual = SignatureBuilder::build(
            'v0',
            1555242631,
            ['moo' => 'cow'],
            'some-other-secret'
        );

        $this->assertSame('v0=ba734dcbfb2063950bb348f6fb053c48ee04cda2ea6501bc7c3c2aec16365403', $actual);
    }
}
