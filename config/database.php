<?php declare(strict_types=1);

return [
    'redis' => [
        'cluster' => env('REDIS_CLUSTER', false),
        'client' => 'predis',
        'default' => [
            'host' => env('REDIS_HOST'),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DATABASE', 0),
            'password' => env('REDIS_PASSWORD', null),
        ],
    ],
];
