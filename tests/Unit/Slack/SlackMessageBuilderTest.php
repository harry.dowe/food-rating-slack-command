<?php declare(strict_types=1);

namespace Tests\Unit\Slack;

use App\FoodHygiene\Establishment;
use App\FoodHygiene\EstablishmentsRepository;
use App\Slack\SlackMessageBuilder;
use Psr\Log\LoggerInterface;
use Tests\TestCase;

class SlackMessageBuilderTest extends TestCase
{
    public function testItBuildsSlackMessage(): void
    {
        $logger = \Mockery::mock(LoggerInterface::class)->shouldIgnoreMissing();

        $repository = \Mockery::mock(EstablishmentsRepository::class);
        $repository->shouldReceive('findAllByCoordinates')
            ->with('KFC', 52.52, -4.6)
            ->andReturn([
                (new Establishment())->setId(555)->setName('KFC')->setRating(5),
            ]);

        $locations = [
            'curly-land' => [
                'latitude' => 52.52,
                'longitude' => -4.6,
            ],
        ];

        $builder = new SlackMessageBuilder($repository, $locations, $logger);

        $slackMessage = $builder->build('KFC', 'curly-land');

        $this->assertSame([
            'response_type' => 'ephemeral',
            'text' => 'We found 1 establishments.',
            'blocks' => [
                [
                    'type' => 'section',
                    'text' => [
                        'type' => 'mrkdwn',
                        'text' => '<https://ratings.food.gov.uk/business/555|KFC> has a rating of 5.',
                    ],
                ],
            ],
        ], $slackMessage->toArray());
    }
}
